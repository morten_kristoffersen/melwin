package no.nlf.fallskjermseksjonen.consumerServices.support;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class MelwinSOAPWSConfiguration {

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("melwin.wsdl");
        return marshaller;
    }

    @Bean
    public MelwinClient melwinClient(Jaxb2Marshaller marshaller) {
        MelwinClient client = new MelwinClient();

        client.setDefaultUri("http://melwin.nak.no/4dwsdl/doc");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }
}
