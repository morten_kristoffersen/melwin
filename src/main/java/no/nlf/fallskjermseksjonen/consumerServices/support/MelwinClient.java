package no.nlf.fallskjermseksjonen.consumerServices.support;

import melwin.wsdl.WsGetAllClubMemberData;
import melwin.wsdl.WsLisenser;
import melwin.wsdl.WsLisenserResponse;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.Member;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import melwin.wsdl.WsGetAllClubMemberDataResponse;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import java.util.*;

@Service
public class MelwinClient extends WebServiceGatewaySupport {

    private static final String MELWIN_CALLBACK = "http://melwin.nak.no/4dwsdl/doc";
    private static final String MELWIN_URI = "http://melwin.nak.no/4DSOAP/DOC";

    public Set<Member> retriveClubMembers(Club club) {
        Set<Member> members = new TreeSet<>();

        WsGetAllClubMemberDataResponse wsResponse = retrieveClubMembersFromMelwin(club.getClubMelwinId());

        for (int index = 0; index < wsResponse.getAClubMembers().getItem().size(); index++) {
            Member member = new Member();
            member.setMemberMelwinId(wsResponse.getAClubMembers().getItem().get(index));
            if (wsResponse.getADateOfBirth().getItem().get(index)!=null) {
                member.setBirthDate(wsResponse.getADateOfBirth().getItem().get(index).toGregorianCalendar().toZonedDateTime().toLocalDate());
            }
            member.setName(wsResponse.getAFornavn().getItem().get(index) + " " + wsResponse.getAEtternavn().getItem().get(index));
            member.setEmail(wsResponse.getAEpost().getItem().get(index));
            member.setPhone(wsResponse.getAMobil().getItem().get(index));
            member.addClub(club);

            members.add(member);
        }

        return members;
    }

    public Map<String, List<String>> retrieveLicenses(Club club, String melwinId, String melwinPassword) {
        Map<String, List<String>> licenses = new TreeMap<>();

        WsLisenserResponse wsResponse = retrieveLicenses(club.getClubMelwinId(), melwinId, melwinPassword);

        for(int index = 0; index < wsResponse.getANummer().getItem().size(); index++) {
            if(licenses.containsKey(wsResponse.getANummer().getItem().get(index))) {
                licenses.get(wsResponse.getANummer().getItem().get(index))
                        .add(wsResponse.getARettighet().getItem().get(index));
            }
            else {
                List<String> licenseList = new ArrayList<>();
                licenseList.add(wsResponse.getARettighet().getItem().get(index));
                licenses.put(wsResponse.getANummer().getItem().get(index), licenseList);
            }
        }
        return licenses;
    }

    private WsGetAllClubMemberDataResponse retrieveClubMembersFromMelwin(String clubMelwinId) {
        WsGetAllClubMemberData request = new WsGetAllClubMemberData();
        request.setVClubID(clubMelwinId);

        WsGetAllClubMemberDataResponse response =
                (WsGetAllClubMemberDataResponse) getWebServiceTemplate().marshalSendAndReceive(
                        MELWIN_URI,
                        request,
                        new SoapActionCallback(MELWIN_CALLBACK)
                );

        return response;
    }

    private WsLisenserResponse retrieveLicenses(String clubMelwinId, String melwinId, String melwinPassword) {
        WsLisenser request = new WsLisenser();
        request.setVKlubbnr(clubMelwinId);
        request.setVMedlem(melwinId);
        request.setVKode(melwinPassword);

        WsLisenserResponse response =
                (WsLisenserResponse) getWebServiceTemplate().marshalSendAndReceive(
                        MELWIN_URI,
                        request,
                        new SoapActionCallback(MELWIN_CALLBACK)
                );

        return response;
    }
}
