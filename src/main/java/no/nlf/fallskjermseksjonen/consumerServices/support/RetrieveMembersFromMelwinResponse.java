package no.nlf.fallskjermseksjonen.consumerServices.support;

import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;

import java.util.Set;

/**
 * Created by m.kristoffersen on 21.05.2016.
 */
public class RetrieveMembersFromMelwinResponse {
    private Set<Member> members;
    private Set<License> Licenses;

    public RetrieveMembersFromMelwinResponse() {}

    public RetrieveMembersFromMelwinResponse(Set<Member> members, Set<License> licenses) {
        this.members = members;
        Licenses = licenses;
    }

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public Set<License> getLicenses() {
        return Licenses;
    }

    public void setLicenses(Set<License> licenses) {
        Licenses = licenses;
    }
}
