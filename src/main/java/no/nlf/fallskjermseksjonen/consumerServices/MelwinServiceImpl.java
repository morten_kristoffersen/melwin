package no.nlf.fallskjermseksjonen.consumerServices;

import no.nlf.fallskjermseksjonen.consumerServices.support.MelwinClient;
import no.nlf.fallskjermseksjonen.consumerServices.support.RetrieveMembersFromMelwinResponse;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MelwinServiceImpl implements MelwinService {

    private MelwinClient melwinClient;
    @Value("${melwin.userid}")
    private String melwinUserID;
    @Value("${melwin.password}")
    private String melwinPassword;

    @Autowired
    public MelwinServiceImpl(MelwinClient melwinClient) {
        this.melwinClient = melwinClient;
    }

    @Override
    public RetrieveMembersFromMelwinResponse retrieveMembersFromMelwin(List<Club> clubs) {
        RetrieveMembersFromMelwinResponse response = new RetrieveMembersFromMelwinResponse();

        Map<String, Member> members = new TreeMap<>();
        Set<License> uniqueLicenses = new TreeSet<>();

        for(Club club : clubs) {
            Set<Member> clubMemberSet = melwinClient.retriveClubMembers(club);

            Map<String, List<String>> licencesFromClub =
                    melwinClient.retrieveLicenses(club, melwinUserID, melwinPassword);

            for(Map.Entry<String, List<String>> entry : licencesFromClub.entrySet()) {
                for(Member member : clubMemberSet) {
                    for(String license : entry.getValue()) {
                        if (!containsLicense(uniqueLicenses, license)) {
                            uniqueLicenses.add(new License(license));
                        }
                    }

                    if (entry.getKey().equals(member.getMemberMelwinId())) {
                        for (String licenseString : entry.getValue()) {
                            for (License license : uniqueLicenses) {
                                if (license.getLicenseMelwinId().equals(licenseString)) {
                                    member.addLicense(license);
                                }
                            }
                        }
                    }
                }
            }

            for (Member member : clubMemberSet) {
                if (members.containsKey(member.getMemberMelwinId())) {
                    members.get(member.getMemberMelwinId()).addClub(club);
                }
                else {
                    members.put(member.getMemberMelwinId(), member);
                }
            }
        }

        response.setMembers(new TreeSet<>(members.values()));
        response.setLicenses(uniqueLicenses);

        return response;
    }

    private boolean containsLicense(Set<License> licenses, String licenseID) {
        for (License license : licenses) {
            if (license.getLicenseMelwinId().equals(licenseID)) {
                return true;
            }
        }
        return false;
    }
}
