package no.nlf.fallskjermseksjonen.consumerServices;

import no.nlf.fallskjermseksjonen.consumerServices.support.RetrieveMembersFromMelwinResponse;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.Member;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface MelwinService {
    /**
     * @param clubs
     * @return
     */
    public RetrieveMembersFromMelwinResponse retrieveMembersFromMelwin(List<Club> clubs);
}

