package no.nlf.fallskjermseksjonen.services;

import no.nlf.fallskjermseksjonen.consumerServices.MelwinService;
import no.nlf.fallskjermseksjonen.consumerServices.support.RetrieveMembersFromMelwinResponse;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;
import no.nlf.fallskjermseksjonen.restRepositories.ClubRepository;
import no.nlf.fallskjermseksjonen.restRepositories.LicenceRepository;
import no.nlf.fallskjermseksjonen.restRepositories.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

@Component
public class MemberServiceImpl implements MemberService {

    private ClubRepository clubRepository;
    private LicenceRepository licenceRepository;
    private MemberRepository memberRepository;
    private MelwinService melwinService;

    @Autowired
    public MemberServiceImpl(ClubRepository clubRepository, LicenceRepository licenceRepository, MemberRepository memberRepository, MelwinService melwinService) {
        this.clubRepository = clubRepository;
        this.licenceRepository = licenceRepository;
        this.memberRepository = memberRepository;
        this.melwinService = melwinService;
    }

    @Override
    public Set<Member> retrieveMembersFromMelwin() {
        RetrieveMembersFromMelwinResponse response = melwinService.retrieveMembersFromMelwin((List<Club>) clubRepository.findAll());

        licenceRepository.save(response.getLicenses());

        Set<Member> members = response.getMembers();

        memberRepository.save(members);

        return members;
    }
}
