package no.nlf.fallskjermseksjonen.services;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public interface RefreshService {
    @Async
    void refreshDb();
}
