package no.nlf.fallskjermseksjonen.services;


import no.nlf.fallskjermseksjonen.domain.Member;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public interface MemberService {


    public Set<Member> retrieveMembersFromMelwin();
}
