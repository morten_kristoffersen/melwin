package no.nlf.fallskjermseksjonen.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class RefreshServiceImpl implements RefreshService{

    private MemberService memberService;

    @Autowired
    public RefreshServiceImpl(MemberService memberService) {
        this.memberService = memberService;
    }

    @Override
    @Async
    public void refreshDb() {
        memberService.retrieveMembersFromMelwin();
    }
}
