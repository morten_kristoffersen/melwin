package no.nlf.fallskjermseksjonen.domain;

import javax.persistence.*;

@Entity
@Table(name = "License")
public class License implements Comparable<License>{
    @Id
    private String licenseMelwinId;

    public License() {
    }

    public  License(String melwinId) {
        this.licenseMelwinId = melwinId;
    }

    public String getLicenseMelwinId() {
        return licenseMelwinId;
    }

    public void setLicenseMelwinId(String licenseMelwinId) {
        this.licenseMelwinId = licenseMelwinId;
    }

    @Override
    public int compareTo(License license) {
        return this.licenseMelwinId.compareTo(license.getLicenseMelwinId());
    }
}
