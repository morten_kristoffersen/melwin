package no.nlf.fallskjermseksjonen.domain;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "Member")
@Table(name = "Member")
public class Member implements Comparable<Member> {
    @Id
    private String memberMelwinId;
    private String name;
    private LocalDate birthDate;
    @ManyToMany
    private List<Club> clubs = new ArrayList<>();
    private String email;
    private String phone;
    @ManyToMany
    private List<License> licenses = new ArrayList<>();

    public Member() {
    }

    public Member(String memberMelwinId, String name, LocalDate birthDate, List<Club> clubs, String email, String phone, List<License> licenses) {
        this.memberMelwinId = memberMelwinId;
        this.name = name;
        this.birthDate = birthDate;
        this.clubs = clubs;
        this.email = email;
        this.phone = phone;
        this.licenses = licenses;
    }

    public String getMemberMelwinId() {
        return memberMelwinId;
    }

    public void setMemberMelwinId(String memberMelwinId) {
        this.memberMelwinId = memberMelwinId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

    public void addClub(Club club) {
        this.clubs.add(club);
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<License> getLicenses() {
        return licenses;
    }

    public void setLicenses(List<License> licenses) {
        this.licenses = licenses;
    }

    public void addLicense(License license) {
        this.licenses.add(license);
    }

    @Override
    public int compareTo(Member member) {
        if (memberMelwinId != null) {
            return this.getMemberMelwinId().compareTo(member.getMemberMelwinId());
        }
        return -1;
    }
}
