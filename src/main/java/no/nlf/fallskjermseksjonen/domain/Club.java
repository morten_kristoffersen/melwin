package no.nlf.fallskjermseksjonen.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Club")
public class Club {
    @Id
    private String clubMelwinId;
    private String name;

    public Club() {}

    public Club(String clubMelwinId, String name) {
        this.clubMelwinId = clubMelwinId;
        this.name = name;
    }

    public String getClubMelwinId() {
        return clubMelwinId;
    }

    public void setClubMelwinId(String clubMelwinId) {
        this.clubMelwinId = clubMelwinId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
