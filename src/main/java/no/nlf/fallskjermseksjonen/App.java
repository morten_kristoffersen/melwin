package no.nlf.fallskjermseksjonen;

import ch.qos.logback.core.net.SyslogOutputStream;
import no.nlf.fallskjermseksjonen.consumerServices.support.MelwinSOAPWSConfiguration;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.restRepositories.ClubRepository;
import no.nlf.fallskjermseksjonen.services.MemberService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@Import({ MelwinSOAPWSConfiguration.class })
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class);
    }

    @Bean
    CommandLineRunner commandLineRunner(MemberService memberService, ClubRepository clubRepository) {
        return args -> {
            System.out.println("Updating DB started:" + LocalTime.now());
                    clubRepository.save(getClubs());
            memberService.retrieveMembersFromMelwin();
            System.out.println("Updating DB done:" + LocalTime.now());
        };
    }

    private List<Club> getClubs() {
        List<Club> clubs = new ArrayList<>();

        clubs.add(new Club("330-F", "HaGL Fallskjermklubb"));
        clubs.add(new Club("310-F", "Bergen Fallskjermklubb"));
        clubs.add(new Club("311-F", "Bodø Fallskjermklubb"));
        clubs.add(new Club("342-F", "Fallskjermklubben Krigsskolen"));
        clubs.add(new Club("322-F", "Føniks Fallskjermklubb"));
        clubs.add(new Club("325-F", "Grenland Fallskjermklubb"));
        clubs.add(new Club("335-F", "Hønefoss Fallskjermklubb"));
        clubs.add(new Club("340-F", "Kjevik Fallskjermklubb"));
        clubs.add(new Club("344-F", "Lesja Fallskjermklubb"));
        clubs.add(new Club("350-F", "Nimbus Fallskjermklubb"));
        clubs.add(new Club("351-F", "NTNU Fallskjermklubb"));
        clubs.add(new Club("360-F", "Oslo Fallskjermklubb"));
        clubs.add(new Club("364-F", "Rana FSK"));
        clubs.add(new Club("370-F", "Stavanger Fallskjermklubb"));
        clubs.add(new Club("380-F", "Troms Fallsjermklubb"));
        clubs.add(new Club("332-F", "Tromsø Fallskjermklubb"));
        clubs.add(new Club("375-F", "Tønsberg Fallskjermklubb"));
        clubs.add(new Club("323-F", "Veteranenes Fallskjermklubb"));
        clubs.add(new Club("391-F", "Voss Fallskjermklubb"));
        clubs.add(new Club("392-F", "Voss Kroppsfykarlag"));
        return clubs;
    }
}

