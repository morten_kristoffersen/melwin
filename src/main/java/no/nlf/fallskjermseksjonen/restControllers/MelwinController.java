package no.nlf.fallskjermseksjonen.restControllers;

import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.services.RefreshService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@EnableAsync
public class MelwinController {

    private RefreshService refreshService;

    @RequestMapping("/ping")
    public String ping() {
        return "pong";
    }

    @RequestMapping("/refresher")
    public String update() throws ExecutionException, InterruptedException {

        LocalDateTime startTime = LocalDateTime.now();
        refreshService.refreshDb();

        return "refresh started: " +  startTime.format(DateTimeFormatter.ISO_LOCAL_TIME).toString();
    }

    @Autowired
    public void setRefreshService(RefreshService refreshService) {
        this.refreshService = refreshService;
    }

    private List<Club> getClubs() {
        List<Club> clubs = new ArrayList<>();

        clubs.add(new Club("330-F", "HaGL Fallskjermklubb"));
        clubs.add(new Club("310-F", "Bergen Fallskjermklubb"));
        clubs.add(new Club("311-F", "Bodø Fallskjermklubb"));
        clubs.add(new Club("342-F", "Fallskjermklubben Krigsskolen"));
        clubs.add(new Club("322-F", "Føniks Fallskjermklubb"));
        clubs.add(new Club("325-F", "Grenland Fallskjermklubb"));
        clubs.add(new Club("335-F", "Hønefoss Fallskjermklubb"));
        clubs.add(new Club("340-F", "Kjevik Fallskjermklubb"));
        clubs.add(new Club("344-F", "Lesja Fallskjermklubb"));
        clubs.add(new Club("350-F", "Nimbus Fallskjermklubb"));
        clubs.add(new Club("351-F", "NTNU Fallskjermklubb"));
        clubs.add(new Club("360-F", "Oslo Fallskjermklubb"));
        clubs.add(new Club("364-F", "Rana FSK"));
        clubs.add(new Club("370-F", "Stavanger Fallskjermklubb"));
        clubs.add(new Club("380-F", "Troms Fallsjermklubb"));
        clubs.add(new Club("332-F", "Tromsø Fallskjermklubb"));
        clubs.add(new Club("375-F", "Tønsberg Fallskjermklubb"));
        clubs.add(new Club("323-F", "Veteranenes Fallskjermklubb"));
        clubs.add(new Club("391-F", "Voss Fallskjermklubb"));
        clubs.add(new Club("392-F", "Voss Kroppsfykarlag"));
        return clubs;
    }

}
