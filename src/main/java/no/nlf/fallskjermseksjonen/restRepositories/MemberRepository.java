package no.nlf.fallskjermseksjonen.restRepositories;

import no.nlf.fallskjermseksjonen.domain.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "members", path = "members")
public interface MemberRepository extends JpaRepository <Member, String> {

    List<Member> findByClubs_ClubMelwinId(@Param("clubMelwinId") String clubMelwinId);

    List<Member> findByLicenses_LicenseMelwinId(@Param("licenseMelwinId") String licenseMelwinId);

    List<Member> findDistinctByClubs_ClubMelwinIdAndLicenses_LicenseMelwinId(@Param("clubMelwinId") String clubMelwinId,@Param("licenseMelwinId") String licenseMelwinId );

    @Override
    @RestResource(exported = false)
    void delete(Iterable<? extends Member> entities);

    @Override
    @RestResource(exported = false)
    void delete(String s);

    @Override
    @RestResource(exported = false)
    void delete(Member entity);

    @Override
    @RestResource(exported = false)
    <S extends Member> List<S> save(Iterable<S> entities);

    @Override
    @RestResource(exported = false)
    <S extends Member> S save(S entity);

    @Override
    @RestResource(exported = false)
    <S extends Member> S saveAndFlush(S entity);
}
