package no.nlf.fallskjermseksjonen.restRepositories;

import no.nlf.fallskjermseksjonen.domain.Club;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "clubs", path = "clubs")
public interface ClubRepository extends JpaRepository<Club, String> {
    @Override
    @RestResource(exported = false)
    void delete(Iterable<? extends Club> entities);

    @Override
    @RestResource(exported = false)
    void delete(String s);

    @Override
    @RestResource(exported = false)
    void delete(Club entity);

    @Override
    @RestResource(exported = false)
    <S extends Club> List<S> save(Iterable<S> entities);

    @Override
    @RestResource(exported = false)
    <S extends Club> S save(S entity);

    @Override
    @RestResource(exported = false)
    <S extends Club> S saveAndFlush(S entity);


}
