package no.nlf.fallskjermseksjonen.restRepositories;

import no.nlf.fallskjermseksjonen.domain.License;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;


@RepositoryRestResource(collectionResourceRel = "licenses", path = "licenses")
public interface LicenceRepository extends JpaRepository<License, String> {
    @Override
    @RestResource(exported = false)
    void delete(Iterable<? extends License> entities);

    @Override
    @RestResource(exported = false)
    void delete(String s);

    @Override
    @RestResource(exported = false)
    void delete(License entity);

    @Override
    @RestResource(exported = false)
    <S extends License> List<S> save(Iterable<S> entities);

    @Override
    @RestResource(exported = false)
    <S extends License> S save(S entity);

    @Override
    @RestResource(exported = false)
    <S extends License> S saveAndFlush(S entity);
}
