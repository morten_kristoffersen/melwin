package no.nlf.fallskjermseksjonen.services;


import no.nlf.fallskjermseksjonen.consumerServices.MelwinService;
import no.nlf.fallskjermseksjonen.consumerServices.support.RetrieveMembersFromMelwinResponse;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;
import no.nlf.fallskjermseksjonen.restRepositories.ClubRepository;
import no.nlf.fallskjermseksjonen.restRepositories.LicenceRepository;
import no.nlf.fallskjermseksjonen.restRepositories.MemberRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MemberServiceImplTest {

    @Mock
    private ClubRepository clubRepositoryMock;
    @Mock
    private LicenceRepository licenceRepositoryMock;
    @Mock
    private MemberRepository memberRepositoryMock;
    @Mock
    private MelwinService melwinServiceMock;
    @InjectMocks
    private MemberService memberService;

    @Before
    public void setUp() {
        memberService =  new MemberServiceImpl(clubRepositoryMock, licenceRepositoryMock, memberRepositoryMock, melwinServiceMock);
        initMocks(this);
    }

    @Test
    public void shouldRetrieveMembersFromMelwin() throws Exception {

        final Set<Member> members = createTestMemberSet();
        final Set<License> licenses = createTestLicenseSet();

        List<Club> clubs = createTestClubsList();

        RetrieveMembersFromMelwinResponse response = new RetrieveMembersFromMelwinResponse(members, createTestLicenseSet());
        when(clubRepositoryMock.findAll()).thenReturn(clubs);
        when(melwinServiceMock.retrieveMembersFromMelwin(clubs)).thenReturn(response);

        Set<Member> resultSet = memberService.retrieveMembersFromMelwin();

        assertEquals(resultSet, members);
        verify(clubRepositoryMock).findAll();
        verify(melwinServiceMock).retrieveMembersFromMelwin(clubs);
        verify(licenceRepositoryMock).save(licenses);
        verify(memberRepositoryMock).save(members);
    }

    private List<Club> createTestClubsList() {
        List<Club> testClubs = new ArrayList<>();

        testClubs.add(new Club("330-F", "HaGL FSK"));
        testClubs.add(new Club("360-F", "Oslo FSK"));
        testClubs.add(new Club("340-F", "Kjevik FSK"));

        return testClubs;
    }

    private List<License> createTestLicenseList() {
        List<License> testLicenses =  new ArrayList<>();

        testLicenses.add(new License("F-B"));
        testLicenses.add(new License("F-C"));
        testLicenses.add(new License("F-D"));
        return testLicenses;
    }

    private Set<License> createTestLicenseSet() {
        Set<License> testLicenses = new TreeSet<>();

        testLicenses.add(new License("F-B"));
        testLicenses.add(new License("F-C"));
        testLicenses.add(new License("F-D"));
        return testLicenses;
    }

    private Set<Member> createTestMemberSet() {
        Set<Member> testMembers = new TreeSet<>();

        testMembers.add(new Member("12345","Per Johansen", LocalDate.now(), createTestClubsList(), "mail@mail.com", "12345678", createTestLicenseList()));
        return testMembers;
    }
}