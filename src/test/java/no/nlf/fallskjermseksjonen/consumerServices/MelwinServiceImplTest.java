package no.nlf.fallskjermseksjonen.consumerServices;

import no.nlf.fallskjermseksjonen.consumerServices.support.MelwinClient;
import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MelwinServiceImplTest {

    @Mock
    private MelwinClient melwinClientMock;

    @InjectMocks
    private MelwinService melwinService;

    @Before
    public void setUp() {
        melwinService = new MelwinServiceImpl(melwinClientMock);
        initMocks(this);
    }

    @Test
    public void shouldRetrieveMembersFromMelwin() throws Exception {
        List<Club> testClubs = createTestClubsListThreeClubs();

        when(melwinClientMock.retriveClubMembers(testClubs.get(0))).thenReturn(createTestMemberSetWithOneMember());
        //when(melwinClientMock.retrieveLicenses())

        melwinService.retrieveMembersFromMelwin(testClubs);
    }

    private List<Club> createTestClubsListThreeClubs() {
        List<Club> testClubs = new ArrayList<>();

        testClubs.add(new Club("330-F", "HaGL FSK"));
        testClubs.add(new Club("360-F", "Oslo FSK"));
        testClubs.add(new Club("340-F", "Kjevik FSK"));

        return testClubs;
    }

    private List<Club> createTestClubsListOneClub() {
        List<Club> testClubs = new ArrayList<>();

        testClubs.add(new Club("330-F", "HaGL FSK"));
        return testClubs;
    }

    private Set<Member> createTestMemberSetWithOneMember() {
        Set<Member> testMembers = new TreeSet<>();

        testMembers.add(new Member("12345","Per Johansen", LocalDate.now(), createTestClubsListThreeClubs(), "mail@mail.com", "12345678", new ArrayList<License>()));
        return testMembers;
    }

    private Set<Member> createTestMemberSetWithTwoMembers() {
        Set<Member> testMembers = new TreeSet<>();

        testMembers.add(new Member("12345","Per Johansen", LocalDate.now(), createTestClubsListThreeClubs(), "mail@mail.com", "12345678", createTestLicenseList()));
        testMembers.add(new Member("12453","Ola Nordmann", LocalDate.now().plusYears(1), createTestClubsListOneClub(), "mail@mail.com", "12345678", createTestLicenseListonlyBLicense()));
        return testMembers;
    }

    private List<License> createTestLicenseListonlyBLicense() {
        List<License> testLicenses =  new ArrayList<>();
        testLicenses.add(new License("F-B"));
        return testLicenses;
    }


    private List<License> createTestLicenseList() {
        List<License> testLicenses =  new ArrayList<>();

        testLicenses.add(new License("F-B"));
        testLicenses.add(new License("F-C"));
        testLicenses.add(new License("F-D"));
        return testLicenses;
    }

    private Set<License> createTestLicenseSet() {
        Set<License> testLicenses = new TreeSet<>();

        testLicenses.add(new License("F-B"));
        testLicenses.add(new License("F-C"));
        testLicenses.add(new License("F-D"));
        return testLicenses;
    }
}