package no.nlf.fallskjermseksjonen.domainBuilders;

import no.nlf.fallskjermseksjonen.domain.Club;
import no.nlf.fallskjermseksjonen.domain.License;
import no.nlf.fallskjermseksjonen.domain.Member;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class MemberBuilder {

    private String memberMelwinId;
    private String name;
    private LocalDate birthDate;
    private List<Club> clubs = new ArrayList<>();
    private String email;
    private String phone;
    private List<License> licenses = new ArrayList<>();

    public MemberBuilder withMemberMelwinId(String memberMelwinId) {
        this.memberMelwinId = memberMelwinId;
        return this;
    }

    public MemberBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public MemberBuilder withBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public MemberBuilder withClubs(List<Club> clubs) {
        this.clubs = clubs;
        return this;
    }

    public MemberBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    private MemberBuilder withLicenses(List<License> licenses) {
        this.licenses = licenses;
        return this;
    }

    public Member build() {
        return new Member(memberMelwinId, name, birthDate, clubs, email, phone, licenses);
    }
}
