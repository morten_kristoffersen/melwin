package no.nlf.fallskjermseksjonen.domainBuilders;

import no.nlf.fallskjermseksjonen.domain.License;

public class LicenseBuilder {

    private String licenseMelwinId;

    public LicenseBuilder withLicenseMelwinId(String licenseMelwinId) {
        this.licenseMelwinId = licenseMelwinId;
        return this;
    }

    public License build() {
        return new License(licenseMelwinId);
    }
}
