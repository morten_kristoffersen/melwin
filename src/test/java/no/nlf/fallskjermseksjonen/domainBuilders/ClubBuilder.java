package no.nlf.fallskjermseksjonen.domainBuilders;

import no.nlf.fallskjermseksjonen.domain.Club;

public class ClubBuilder {

    private String clubMelwinId;
    private String name;

    public ClubBuilder withClubMelwinId(String clubMelwinId) {
        this.clubMelwinId = clubMelwinId;
        return this;
    }

    public ClubBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public Club build() {
        return new Club(clubMelwinId, name);
    }
}
