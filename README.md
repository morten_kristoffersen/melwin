# README #

*Melwin Proxy* is a soap web service client that consumes some of the methods from *melwin.nak.no*. The data is stored in an "in memory" database. The data is exposed in a REST-API.

Whole app is inspired by this presentation by Josh Long from Javazone 2015:
https://vimeo.com/138957694

### What is this repository for? ###

* MELWIN PROXY
* Version 1.0-SNAPSHOT

### REST Resources ###

Resources that can explored in the HAL Browser (host/browser). Has pagination and seach filters.

* host/members/
* host/clubs/
* host/licenses/

Other resources.

* host/ping, (Answer is pong) For healtcheck and waking the app up i deployed on Heroku
* host/refresher, Updates the in memory db with data from melwin. is asynchronous fire and forget. 

### How do I get set up? ###

* Summary of set up, you will need java 8 jdk, both oracles and open-jdk can be used and Maven 3 

* Configuration
Go to the project folder where the _pom.xml_ is located and run _mvn clean install_.
If the build is completed, go to the _target_ folder. And then run _java -jar -Dmelwin.userid=<<melwin_member_number>>,melwin.password=<<melwin_password>> melwin-proxy-<<version>>.jar_
remember to replace _melwin_member_number_ and _melwin_password_ with "fagsjefens" melwin credentials.
The app will retrieve all data from melwin at startup. Takes normally from 5 to 8 minutes.
when you can see "updating DB done" in the console, and "no.nlf.fallskjermseksjonen.App : Started App in XX.XX seconds (JVM running for XX.XX)"
you can explore the rest API from "http://localhost:8080/browser"
* Dependencies
The application uses spring boot 1.3.4.RELEASE, H2 db, junit 4 and mockito 1.10 
* Database configuration
None, the DB is embedded in the app
* How to run tests
Normal maven workflow
* Deployment instructions
The jar in the target folder can be run everywhere with a java 8 jvm. The application uses a embedded Tomcat webserver.

### KNOWN TODOS ###

* Rest api security, everything is open.
* Test coverage, none.  If any students I'm mentoring see this repo, Do as i say, Not what I do!
* Versioning of rest api
* scheduling of db updates from melwin.